---
author: "Blas Simon"
tags: ci
date: 2017-05-24 12:45:00+01:00
---

Compte rendu du 24 mai 2017:

# Tournage des vidéos:

Aujourd'hui, on va tourner la vidéo d'introduction de la vidéo du club Info, avec
l'aimable participation de Maëva et Nicolas, qui vont discuter nonchalamment dans 
le couloir puis entrer dans le local. Toutes les vidéos on donc été tournées! 
Merci Eloi!

# Tuto d'Hugo:

[ENFIN! Un lien vers le tuto d'Hugo. En première version, la correction viendra plus tard.](https://archives.plil.fr/LeClubInfo/ci-site/blob/master/_tutos/Tutos_Hugo)

Aujourd'hui, les cartes graphiques:
Elle sont surtout utiles pour les jeux vidéos et les logiciels de modèlisation.


Voila, voila, fin de la réunion, une bonne journée à tous!