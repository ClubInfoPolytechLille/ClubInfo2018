---
date:   2017-04-05 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

# Compte rendu de la réunion du 05-04-2017


## Le point vêtement Justine

.....Un poing vêtement.....

## Kilobits ... C'est bientôt fini ???!!!

Simon a commencé à vérifier les requêtes SQL. Voila. On va ptêt finir bientôt.

## Video de présentation

Mercredi de la rentrée, on fera les vidéos en groupe. Ne mettez pas vos vêtements 
du cub info. Benoit ramènera sa couronne.

## La programmation le Lundi soir ? / Le point Geoffrey

Vendredi, Geoffrey a participé au coding contest. Apparemment c'était bien. :)
Le Lundi soir de 17h00 à 19h00, il y a des entrainements aux concours, qui permettent
d'apprendre des trucs cools, et d'éviter les conneries à la con. Vachement utile
si vous voulez remporter des concours. Pas mal de gens y vont mais pour y aller,
faut sècher.
 
Le BDE va demander au club info  de leur réaliser un site internet (oui enfin) 
ainsi qu'une boutique en ligne pour y vendre des goodies.

Le boutique en ligne sera faite simplement, à l'aide de choses déjà faites.
Hugo et Justine sont sur le coup. Justine a déjà commencé ses recherches, c'est bien. 
Le bde à les droits de vendre des objets d'un point de vue légal, donc tout va bien,
on peut faire une boutique sans se créer de problèmes.

Pour le site du BDE, il faudra leur en reparler, histoire d'être sûrs qu'ils soient intéressés.

## Nouveau Site WEB

On a un nouveau site web! On est heureux.
Voici l'adresse (vers mes compte-rendus): [Le site du club info](https://clubinfo.plil.net/)
Vous pourrez y trouver un tuto git, un tuto d'introduction à l'agorithmique, et 
plein de tutos fais par nos bien aimés présidents.
Vous pouvez également y trouver mes CR, les tutos Hugo, et un site renové et plus beau.
Et surtout, il ne prend plus autant de place, et devrait donc charger un peu plus
rapidement (un peu)(à peine quelques dizaines de secondes)

## Le Tutooooooooooo d'Hugo! Le Tutooooooooo d'Hugo! 

A l'avenir, on aura un générique pour le tuto  d'Hugo.
Toujours pas besoin de le noter, vous pourrez le trouver sur le site du club info.
Cf le tuto d'Hugoooooooo.
On aura des liens plus tard.

Hugo dessine des choses étranges au tableau, et le Club info à beaucoup d'imagination.
Si vous vouliez plus de détails, il fallait venir. Vous avez raté de nombreuses
blagues. Et un jingles. Et des anecdotes. 

'insérer ici le génerique de fin du tuto d'Hugo'

## Projet d'une série appelée: le tuto d'Hugo

Nicolas a pris des photos. On est a 4 chapitres, pour le moment. 
Approuvé par le point vêtements. Affaire à suivre.





FIN DE LA REUNION!