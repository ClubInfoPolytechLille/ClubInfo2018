---
date:   2017-11-22 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

#### Début de la réunion à 12h55

Au programme aujourd'hui:
- Point vêtements
- Photos de club
- ~~Tuto FrontEnd~~ Tuto BackEnd
- Nom de l'équipe de la Nuit de l'Info / Réunion logiciels Nuit de l'Info

### Point vêtements

   Intéressés par un Polo ou un Sweet-shirt du club Info? guettez un post qui
arrivera bientôt sur la page du club Info. On a besoin de connaitre tous les 
intéressés pour pouvoir négocier les prix avec les différents vendeurs. Merci
de votre coopération.

### Photos de club

   Photo du club info le 24 novembre à 10h00 (ce vendredi).
Photos de l'associatif le 6 décembre à 13h45.
Si vous voulez acheter l'une ou l'autre des photos, remplissez le tableur sur 
la page du club Info. Il faut payer avant le 6 décembre ^^. Pour le paiement,
contactez Justine.

### Tuto BackEnd

   Il était prévu de faire un tuto FrontEnd mais le principal intéressé 
(Nicolas) est absent, ce sera donc un tuto BackEnd pour tout le monde!

### Nuit de l'info

   Le nom officiel de l'équipe du club Info pour la Nuit de l'Info sera donc 
la GeoffreyPrésidentTeam (GPT).

   Pour l'installation des logiciels sur vos ordis, ça se passera le samedi
2 décembre à partir de 15h00 chez Benoit! A venir un post pour vous dire comment y 
aller!

#### Voilà voilà fin de la réunion à 13h18, une bonne journée à tous!