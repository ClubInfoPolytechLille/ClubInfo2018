---
date:   2017-11-15 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

### Bonjour à toutes et à tous!

# Un nouveau! Encore! Soit le bienvenu N8! (Il s'appelle Nicolas).

Voici le compte rendu de réunion du 15 Novembre 2017.

Au programme aujourd'hui:
- La nuit de l'info
- Tuto BackEnd

Au programme la semaine prochaine:
- Tuto FrontEnd

# Nuit de l'info

La nuit de l'info, c'est cool.

# Tuto BackEnd

[Tuto BackEnd](https://docs.google.com/presentation/d/1wXd40CeHhXRmhZ2ehGayMq4W4_qhKdY4x599M7D07cg/edit#slide=id.p)

Cependant, il fallait être présent à la réunion pour comprendre ces slides, mais 
si vous étiez absents et que vous n'êtes pas sur la partie BackEnd à la nuit de 
l'Info, dites vous simplement que le BackEnd, c'est simple ^^.

On a donc eu droit à un magnifique tuto BackEnd par Benoit, suivi d'une 
démonstration sur le serveur réalisé lors de la nuit de l'info de l'année dernière <3.

Fin de la Réunion!

Une bonne journée à tous et à toutes! 
