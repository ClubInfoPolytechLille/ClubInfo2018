---
date:   2017-01-18 13:45:00 +0100
author: "Geoffrey Preud'homme"
tags: ci
---

# Meet-up Big Data & Machine Learning #2

Il a lieu demain. Que du Big Data en prévision.
Plus d'infos ici : <https://www.meetup.com/Lille-Big-Data-and-Machine-Learning-Meetup/events/235857735/>

