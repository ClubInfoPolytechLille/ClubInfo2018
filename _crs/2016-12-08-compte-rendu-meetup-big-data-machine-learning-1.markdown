---
title:  "Compte-rendu Meetup Big Data & Machine Learning #1"
date:   2016-12-08 20:30:00 +0100
author: "Geoffrey Preud'homme"
tags: machine-learning big-data meetup cr
---

# Meta

Premier meet-up d’[une
série qui devrait avoir lieu tous les deuxièmes jeudi du mois](https://www.meetup.com/fr-FR/Lille-Big-Data-and-Machine-Learning-Meetup/),
où on parlera technique et fera des retours d’expérience. Les
organisateurs (dont un est prof en GIS à Polytech) sont contents de
faire ça et heureux de nous voir si nombreux.

# Optimisation de YARN

Retour d’expérience de quelqu’un qui a
travaillé pour [Wajam](https://www.wajam.com/),
un service qui récupère les données des réseaux sociaux pour en
faire des recommandations personnelles pertinentes.

Pour faire ça ils utilisent un cluster
(c’est-à-dire un groupe d’ordinateurs physiques rassemblés au
même endroit physiquement) qui tourne sur [Hadoop](https://fr.wikipedia.org/wiki/Hadoop)
(permet de faire du calcul distribué, ici toutes les machines du
cluster contribuent à analyser les données des réseaux sociaux
pour en faire des recommandations).

Sauf qu’un beau jour, problème, au lieu d’avoir
un flux de données entrant de 150 Go/jour, ils se retrouvent à
devoir traiter 300 Go/jour, et il n’y a plus de place pour rajouter
physiquement des machines au cluster. Ils commencent donc à analyser
avec [Cloudera](https://fr.wikipedia.org/wiki/Cloudera) comment se passe le traitement de données notamment la
répartition des tâches sur les machines qui est faite par le
logiciel [YARN](https://fr.wikipedia.org/wiki/Hadoop_YARN).
Ils se rendent compte de plusieurs choses :

*   Certaines tâches [reduce](https://fr.wikipedia.org/wiki/MapReduce)
	échouaient deux fois avant de réussir au troisième essai. Cela
	était dû au fait que seul 1 Go de mémoire vive était alloué
	pour chaque bloc (une grosse tâche est découpée en plusieurs
	petits blocs répartis sur le cluster) alors qu’il avait besoin
	d’un peu plus (si ça marchait à la troisième fois, c’est
	probablement grâce à la [préemption](https://fr.wikipedia.org/wiki/Multit%C3%A2che_pr%C3%A9emptif)
	de YARN : au bout de deux échecs il autorise le bloc à manger
	de la mémoire sur les autres blocs si ils ne l’utilisent pas)

*   La répartition des tâches était trop
	calculée à partir de la mémoire disponible, et ne prenait pas en
	compte le CPU qui parfois était jamais utilisé, parfois beaucoup
	utilisé par plusieurs blocs ce qui ralentissait le traitement des
	données

*   [Sqoop](https://fr.wikipedia.org/wiki/Apache_Sqoop),
	permettant de transférer des données entre les BDD relationnelles
	(MySQL ici) et HDFS (HaDoop FileSystem, système de fichiers
	permettant la répartition « intelligente » des données
	sur les machines du cluster) avait un quota par défaut pour limiter
	le nombre de requêtes par unité de temps.

*   Les données envoyées à MySQL étaient
	envoyées une par une, ce qui prenait un monstre à chaque
	initialisation de requête.

*   Les outils n’étaient pas à jour. Du
	coup ils rataient pas mal d’optimisation sur les différents
	outils qu’ils utilisaient et en plus étaient tombés sur une
	version de YARN avec un bug qui réduisait sans raison la capacité
	de traitement.

*   Parmi les requêtes SQL, il y avait parfois
	des « ORDER BY » qui n’étaient pas nécessaires et
	ralentissaient considérablement le temps d’exécution

*   Certaines données étaient traitées même
	quand il était sû à l’avance qu’elles ne seraient pas
	pertinentes. Les « FILTER » sur les requêtes SQL c’est
	une bonne idée.

*   Le « [mode
	spéculation](https://fr.wikipedia.org/wiki/Ex%C3%A9cution_sp%C3%A9culative) » était activé. Ce mode pense qu’une
	tâche peut potentiellement rater donc elle est lancée sur
	plusieurs machines différentes et on prend les résultats de celle
	qui finit en premier. Si c’est utile pour faire du traitement de
	données en temps réel, ça gâche des ressources inutilement dans
	ce contexte où si une tâche échoue ça ne coûte rien de la
	relancer.

*   La « collocation de données »
	n’était pas activée. Cet outil permet d’informer YARN de
	l’emplacement physique de chaque machine afin de mettre les
	données souvent utilisées ensemble physiquement côte à côte
	pour réduire les temps d’accès.

*   Parfois, sur des machines à 64 Go de
	mémoire vive, seuls 16 Go étaient déclarés et donc utilisés.


Après avoir corrigé tout ça, ils se sont rendu
compte qu’avec le même cluster, ils pouvaient traiter non plus 150
Go mais 700 Go de données par jour, tout en ayant réduit les
ressources utilisées et les temps de calcul.  Du coup pendant 3 ans
ils utilisaient 4 × trop de serveurs, d’électricité,
de climatisation etc. pour … rien. Tout ça pour 10 lignes de
configuration et de code modifiées.

En plus de faire des économies, ça a leur a
permis de démystifier le fonctionnement du cluster, et du coup ils
n’hésitent pas à le redémarrer de temps en temps pour faire de
l’amélioration continue.

# Retour sur Google Cloud Plateform

Ce monsieur a d’abord travaillé chez SFR où
ils utilisaient comme pour Wajam des clusters de machines.
Aujourd’hui, il travaille pour La Redoute, où ils utilisent [Google Cloud
Plateform](https://cloud.google.com/) (c’est du cloud de 3<sup>e</sup> génération), qui
est un autre moyen de faire du traitement de données sur des grosses
tailles.

C’est essentiellement « serverless »,
c’est-à-dire que les serveurs sont créés et détruit en fonction
des besoins. On parle d’auto-scalabilité. Ce n’est pas forcément
utile pour les petits volumes de données (pour traiter un fichier de
120 lignes il va peut-être créer 120 machines), mais ça devient
intéressant pour de gros volumes. En effet, le genre de traitement
de données qui était fait sur un cluster en 3h peut être fait en
18 secondes avec GCP.

Google Cloud Plateform propose [d’autres services](https://cloud.google.com/products/), qui requièrent pas ou peu de configuration ou de mise
en place :

*   Storage : stockage de fichier, qui du
	coup est persistent comparé aux serveurs

*   Pub / Sub : Système de publication /
	souscription qui permet de connecter les services ensembles de
	manière asynchrone avec des évènements

*   Dataflow : Traitement de donnée en
	temps réél

*   Dataproc : Cluster de machines sur
	Hadoop à la demande

*   BigQuery : Permet de faire de
	l’analyse sur des grosses quantité de données

*   App Engine : Permet d’héberger des
	applications (site web, serveur de jeu…)

*   Machine Learning : Service de Machine
	Learning _(malheurseuement il a pas dit grand-chose là dessus)_

*   Datalog :
	Service de log (journal de tout ce qui a été effectué) amélioré,
	permettant ainsi d’analyser l’utilisation même de GCM
	rapidement

*   …

Les avantages sont
multiples : tout est beaucoup plus simple et plus rapide à
mettre en place, il y a plus de services disponibles qui n’auraient
pas été possibles avec un cluster, les performances sont clairement
meilleures, plus de maintenance de machines physique à faire, la
facturation est faite en fonction de ce qui est réellement utilisé
(ça peut être pratique quand on a pas besoin de faire du traitement
24h/24 par exemple).

Les désavantages sont présents aussi : c’est probablement plus
cher d’utiliser ce service que de faire un cluster Hadoop à la main
avec du matériel de récup, et ça peut être un problème
d’externaliser les données sur un service indépendant pour des
données sensibles.

_Il y a certainement d’autres arguments
contre, mais le monsieur insiste sur le fait que c’est juste son
avis. Essentiellement il n’y a pas de « le cloud c’est
mieux » ou « les clusters c’est la vie », ça
dépend du cas d’utilisation._
