---
date:   2017-12-13 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

## Bienvenus pour le compte rendu de ce retour sur la nuit de l'info de la réunion du 13 décembre 2017

### Bonjour à toutes et à tous!

Il apparaitrait qu'a 404 euros par minute, il n'y a pas de temps d'appel pour la hotline Justine ...

Au programme aujourd'hui:

- Retour ndl

- Parlons des erreur de la ndl (je vous jure c'est marrant)

- Point vêtements

- Point photo

- Point site du Bde

- A venir

## Retour de NDL

On a très bien travaillé, avec 60% de commits en plus par rapport à l'année dernière! De plus, les commits de l'année dernière se sont avèrés relativement inutiles,
donc on a approximativement fait 60 fois plus de commits que l'année dernière.

Réalisation du site au final:

- 70% bdd.

- 40-50% back end.

- Front end problèmatique, benoit était très occupé à aider les autres membres de l'équipe.

10% de l'application fini au final. Mais on avait un début d'application, relativement fonctionnel.

Cette nuit s'est au final plutôt bien passée, malgré quelques petits soucis.

quelques détails:

- Le studio s'est un peu ennuyé, car la majeure partie des gens sont partis vers 2h00 du matin, avec de bonnes raisons. Le studio a fait trop d'interviews?

- La matinée après la NDL n'était pas banalisée, ce qui est un gros soucis.

- Tout le monde n'avait pas ses logiciels à jour, ce qui a nous a ralenti et causé des problèmes.

- On n'a pas eu de problèmes d'internet, ni de problèmes de logiciels dans l'ensemble, ce qui nous a bien aidé.

- Vos bien aimés responsables ont oublié de faire le rendu, ce qui est un peu triste. On est désolés.

- On aurait du passer plus de temps sur la création de la base de données, qu'on a du modifier en cours de route, ce qui nous a pris pas mal de temps.

- Penser à mettre les liens des Read Me et de la procédure d'installation sur le site du Club Info, ainsi que les tutos.

- L'install party était très efficace, mais tout le monde ne peut pas y venir, d'où les tutos sur internet, plus nécessaires.

- Les posts its, ça marche vachement bien, à garder pour l'année prochaine.

- Les gens ont globalement apprécié la répartition du travail, tout le monde pu avancer en géneral.

Le grand bug de 23h30, une grosse pause qui a été nécessaire avec l'ajout des Foreign Keys. Elle nous a beaucoup ralenti et a démotivé pas mal de gens,
ne rien faire n'est semble-t-il, pas motivant. Notre base de données n'a pas supporté l'apparition de Foreign Keys, et détecter l'erreur a été compliqué.

#### On peut être fiers de nous.

La nuit de l'info, c'est surtout un évènement pour être tous ensembles et s'amuser; et une fois encore c'était sympa.


## Point vêtements

Personne n'est intéressé.

## Point photo

Tout le monde à payé, tout va bien!

## Point site BDE

Faut le faire. Voila c'est cool.

## A venir

On va essayer de faire plus de projets, histoire de motiver plus les gens pour le club info. Par exemple, des concours ou un projet loufoque en interne.

#### Voila voila, fin de la réunion. Une bonne journée à tous! :D