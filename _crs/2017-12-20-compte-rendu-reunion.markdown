---
date:   2017-12-20 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

# Réunion du 20 décembre 2017

## Dernière réunion de l'année!

### Membres présents:

- Benoit (préz)

- Nicolas (respo com)

- Moi (secrètaire)

- Hugo (vice-préz)

- Justine (tréz)

- De nombreuses personnes ne participant pas à la réunion
    
## Ordre du jour:

- Emploi du temps pour le projet l'école

- Nouveau Projet (je vous raconte)

- Nouveau Projet

- Idées pour le projet

- Projet projet projet projet projet

- Goodies du club info 

### Emploi du temps pour le projet école

Il faut voir qui serait disponible à la deuxième semaine de rentrée pour aller dans une école pour le projet école. 
Pour l'instant Hugo et Nicolas sont disponibles grâce à la SA.

### Nouveau projet

On a un nouveau projet. Ca sera un concours, constitué de défis. 
5-10 défis, accessibles avec le compte Polytech des gens. On louerait notre 
propre serveur, et on aurait notre propre base de donnée.
Le premier a remplir les défis gagne un lot, qui serait plutôt pas mal.
Cadeau: disque dur de 2To et un coca?
Il y aurait des instructions et des indices sur comment résoudre le défi.
Pour lancer le défi, faire une vidéo de présentation.
Il nous faudra qqn qui sache faire une base de données, on fera ça en Spring.

### Idées pour le projet

- Défi numéro 0: Aimer la page du club Info 

- Défi numéro 1: Appuyer sur F12 pour trouver comment inspecter une page 

- Défi numéro 2: Cacher 5 caractères dans la vidéo de la NDL qui font un mdp à rentrer.

- Défi numéro 3: Rentrer le nom de l'équipe NDL de l'année 2017.

- Défi numéro 4: Konami Code qui serait le numéro de la salle du club info (E301).

- Défi garou (pas le chanteur) numéro 5: Accessible seulement la nuit, si tu y accède, tu le réussi.

- Défi numéro 6: Labyrinthe de liens : La suite à suivre est la suite des noms des présidents du club info.

- Défi numéro 7: Trouver un bouton blanc sur fond blanc sur une page web.

- Défi numéro 8: Demander aux gens de trouver un fichier sur le compte du club info

- Défi numéro 9: Parlez ami et entrez : Porte de la moria, logo anneau. Envoi de fichier audio pour répondre.

### Des goodies du club info

Ecocups, Clés usb, Bonnets du club Info. Ca serait sympa.

## Bonnes vacances à tous! :D

Fin de la réunion.