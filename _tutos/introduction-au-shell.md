---
title: Introduction au shell
tags: shell bash
author: "Geoffrey Preud'homme"
date: 2016-01-06 11:33:35+01:00
---
# Introduction au shell

Le shell, c'est le programme qui fait le pont entre la console, le truc avec des écritures blanches sur fond noir, et les programmes du systme d'exploitation. Mais ici je vais utiliser (à tort) « shell »  comme un mot un peu fourre-tout pour console, shell (le vrai), et les programmes.

Pourquoi manipuler les programmes du système avec un vieux truc pas agréable à regarder et compliqué à utiliser alors qu'on a des belles interfaces graphiques ? Trois raisons :

 1. On peut les éxécuter à distance (avec SSH par exemple) plus simplement et plus facilement qu'avec une solution lourde de partage d'écran (comme VNC par exemple)  
 2. C'est programmable. Vous passerez moins de temps à chercher et construire la commande pour renommer des images selon leur lieu et date de prise de vue qu'à les renommer à la main ou trouver un programme qui fasse exactement ça sur le net, si tenté qu'il existe.
 3. C'est la classe quoi ! Ok vous passez pour un geek mais vous avez le swag ! (Bon ok, ça fait que deux raisons)

Ah, on me signale dans mon oreillette que j'ai déjà passé trop de temps à blablater sur la théorie, passons tout de suite à la pratique.

## Commandes usuelles
(garnies avec leurs exemples)

 - `cd dossier`, `cd ..` : changer de dossier
 - `pwd`: savoir dans quel dossier on est
 - `ls` : lister le contenu du dossier
 - `cat fichier` afficher le contenu du fichier
 - `less fichier` afficher le contenu du fichier de manière plus pratique (touches : `g` : aller au début du fichier, `G` à la fin, `/` puis du texte puis entrée : chercher dans le fichier, `n` prochaine occurence du texte, `N` précédente occurence, `q` quitter)
 - `grep pattern fichier` cherche la ligne qui contient `pattern`dans `fichier`
 - `touch fichier` : créer un fichier vide (utilité++)
 - `cp a b` copier le fichier `a` vers un fichier `b`
 - `cp -r a b` copier le **dossier** `a` vers un fichier `b`
 - `mv a b` déplacer le fichier `a` vers un fichier `b`
 - `rm fichier` supprimer le fichier
 - `zip`, `unzip` manipuler des fichiers `.zip`
 - `rm -r dossier` supprimer le dossier et son contenu (faites gaffe avec ça) 
 - `mkdir dossier` créer un dossier
 - `find Dossier -type f -name lol` trouve tous les fichier qui s'appellent `lol` dans `Dossier`
 - `wget http://leclubinfocestcool.fr` récupérer et enregister une page web
 - `curl http://leclubinfocestcool.fr` récupérer et **afficher** une page web
 - `ssh` se connecter sur une autre machine (voir [le tuto](http://leclubinfocestcool.plil.net/forum/conv/564f17883dd18e6b2c91f06d))
 - `startx -- :1` démarrer une session graphique (aka le saint Graal des PC bloqués car coupés électriquement)
 - `gcc -c main.c -o main` compiler un fichier C
 - `eject` faire sortir une technologie assez agée d'une technologie plus récente
 - `lpstat -a` : lister le nom des imprimantes (bonne chance pour trouver la salle)
 - `lpr -P Gutenberg fichier.pdf` : imprimer sur Gutenberg (méthode non reconnue par le service info :P)
 - `wakeonlan adressemac` : réveiller un PC éteint (la liste des adresses mac des PC de Polytech est sur le Twiki, mais allez pas vous attirer les foudres du service info, surtout que je vous apprend comment faire des boucles après X) )
 - `nano fichier` : éditer un fichier texte, de manière plutôt intuitive
 - `vim fichier` : éditer un fichier texte quand on est barbu (pour ce sortir de ce merdier : <kbd>Échap</kbd>, <kbd>:</kbd>, <kbd>q</kbd>, <kbd>!</kbd>, <kbd>Entrée</kbd>) (vous pouvez aussi utiliser les mêmes touches que dans `less`)
 - `emacs fichier` : éditer un fichier quand on est un autre type de barbu (pour ce sortir de ce merdier : <kbd>Ctrl</kbd>+<kbd>X</kbd>, <kbd>Ctrl</kbd>+<kbd>C</kbd>) (je vous cache pas qu'il y a une petite guéguérre type guerre de religions entre les éditeurs :D)
 - `history` : voir la liste des commandes qu'on a tapé dans le shell
 - `history -c` : effacer son historique de shell-pr0n
 - `exit` ou `logout` : dire bye bye
 -  (on en avait dites d'autres à la réu, mais je les ai oubliées)

On terminera par le St-Graal des programmes systèmes, le `man` (vous pouvez aussi utiliser les mêmes touches que dans `less`). Quand on vous dit d'appliquer le protocole RTFM, c'est ici qu'il faut chercher. 

## La puissance du bash

Le shell utilisé sur les PC de Poly est bash, et croyez-moi, on peut faire achement de choses avec, même si la syntaxe est assez déroutate pour le moins qu'on puisse dire.

### Les pipes
Quand un programme sort du texte, vous pouvez le passer au programme suivant pour peu qu'il sache quoi en faire

 - `ls Documents | grep banane` donne la liste des fichiers dans le dossier Documents dont le nom contient `banane`
 - `cat fichier | sort | uniq -d` lit le fichier, le trie pour finalement afficher les lignes en double

### Les redirections vers un fichier

- `ls Documents > fichier` enregistre les noms des fichiers dans le dossier Documents dans le fichier `fichier` (s'il n'existe pas, il est crée, s'il existe, il est écrasé)
- `echo "Hmmm, pastèque" >> fichier` rajoute la ligne « Hmmm, pastèque » dans le fichier `fichier`

### Les variables

- `fruit=orange`, `prefere="le piment"`, `i=0` assigner une variable
- `find . -name $fruit`, `echo "J'aime $prefere"`

[TO BE CONTINUED]
