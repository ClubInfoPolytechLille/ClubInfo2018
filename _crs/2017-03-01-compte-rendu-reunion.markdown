---
date:   2017-03-01 13:45:00 +0100
author: "Geoffrey Preud'homme"
tags: ci
---

# Trello ne marchait plus hier soir ...
... et il n'y avait pas que lui. Tous les sites qui se basaient sur les services web Amazon étaient en panne. Petit exemple de pourquoi, étant donné qu'une grande partie d'internet repose sur une poignée de services, on peut faire facilement tomber internet. Ça aurait été un Mattermost à nous, j'aurais pu le corriger, là non, fallait juste attendre. On a pas d'info technique sur l'évènement étant donné qu'il est assez récent, mais si vous êtes interessés faites-le moi savoir.

# Fil rouge vêtements
Donnez l'argent à Justine avant ce week-end. Elle peut pas avancer avec ses sous à elle. On peut pas avancer avec les sous du Club Info non plus. Personne n'a d'argent en fait, c'est triste.

# Afterwork repoussé
Au 7 mars. Rien de nouveau à l'horizon si ce n'est que vous devez vous inscrire [ici](https://framadate.org/znHFWdwavDxT6FkX) pour indiquer vos disponibilités.

# Libération du code de 10p5
C'est à dire l'ancêtre d'Etunicorn. Ça pose de problème à personne, je demande à Jean-Loup si il est ok et je mettrait ça sur GitHub + GitLab avec la [WTFPL](http://www.wtfpl.net/).

# Conversation Facebook entre membres
L'idée est d'avoir une conversation foutoir ou on met tous les trucs qui sont pas importants, avec un rapport ou pas avec l'informatique histoire de se détendre un peu, faciliter la cohésion. Libre à chacun de mettre la conversation en sourdine si il a envie.

# Passations

Donc voilà, chacun a exprimé son souhait de poste, et il n'y a pas eu de bataille pour un poste, c'est bien vous êtes sage :)

## Les rôles officiels
- Président : Benoit
- Vice-président : Hugo
- Secrétaire : Simon
- Trésorière : Justine

## Les rôles secondaires
- Respo communication : Nicolas
- Respo Nuit de l'Info : Maëva et Simon
- Respo pull : Justine

## Les responsables de machines
- Serveur Club Info : Geoffrey (le temps qu'on ait une situation stable de ce coté, ça changera par la suite)
- PC 10⁵ : Benoît
- Serveur Studio : Éloi
- Serveur PULCE : Nicolas

## Les rôles indispensables
- Respo absentéisme : Cyprien
- Respo Luxe en Bourgeois : Florian
- Respo Québec : Marianne
- Respo lochon : Baptiste
- Respo robots : Alexis
- Chef de projet web, syteme d'information : Romain
- Respo discours : Florian
- Respo pas d'accord : Justine

On votera officiellement avec les bons papiers la semaine prochaine. Ah et on fera une photo aussi !

