---
author: "Geoffrey Preud'homme"
tags: ci
date: 2015-04-15 12:53:09+02:00
---

À l'ordre du jour :

* Présentation de ce superbe site
* Paperasserie
* Vote du logo à utiliser (Human evolution ou Fractales)
* Reflexions sur la vidéo de présentation et des dates de tournage
* Retour chaleureux d'Erwan
