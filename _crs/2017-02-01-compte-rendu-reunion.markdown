---
date:   2017-02-01 13:45:00 +0100
author: "Geoffrey Preud'homme"
tags: ci
---

# Attribution de missions pour Kilobits

On a réparti les gens sur des missions [Trello](https://trello.com), et on vous a même attribué des deadlines pour vous forcer à bosser, bande de fénéants 😘.

# Soirée de cohésion

L'idée est de se faire une petite soirée avec l'ensemble du club histoire de discuter un peu, d'apprendre à se connaitre tout ça... Indiquez vos disponibilités ici : <https://framadate.org/DPyqARtP3shVIuXx>. Si vous avez des idées de restaurant sympa, n'hésitez pas à proposer ;)

# Vêtements du club

Si vous souhaitez commander des polos aux couleurs du Club Info, c'est maintenant, et faites-le ici : <https://docs.google.com/spreadsheets/d/1vdLQVS8GgNO31eW4FF5_yKrI27C6juS9kYMhYJE6R6g/edit#gid=0>. Date limite de participation : jeudi 10 février.
Justine s'est proposé pour s'occuper de la commande, on la remercie de sa gentillesse ☺.

# Afterwork

On pensait faire un afterwork EESTEC - Robotech - Info un mercredi après les campagnes. Après un rapide sondage, des gens seraient interessés pour participer, ce qui est cool !

# Passations

On en parlera principalement la semaine prochaine en vous décrivant les postes, mais commencez déjà à réflechir si oui ou non vous seriez prêt à sacrifier quelques heures de votre temps pour faire survivre le club le plus cool de tout Polytech !

# Site du Fablab

Le Fablab souhaiterait un site internet. Vu l'engoument général, Romain s'engage « à torcher ça en deux jours » *(si le Fablab lit ceci : c'est une expression, Romain c'est un bon il va vous faire un joli site)*.

# Coding contest

Un concours de programmation qui a lieu sur l'université le 31 mars de 14h à 18h. Si vous êtes interessés : <http://events.codeweek.eu/view/36662/coding-contest-2017/>.

# Meetup #3 Big Data & Machine Learning

Premier meetup avec du machine learning, plus précisément du deep learning pour le coup. Ça se passe jeudi 9 au soir chez OVH. Inscrivez-vous vite, les places sont limitées : <https://www.meetup.com/Lille-Big-Data-and-Machine-Learning-Meetup/events/235857786/?rv=ea1&_af=event&_af_eid=235857786&https=on>.

# Projet terminé : Serveur mail PULCE

Nicolas et Geoffrey s'y sont penché mercredi dernier. Il est beau, il est tout chaud, il est là : <https://mail.pulce.fr/>. Haha, vous pouvez pas y accéder, ce n'est pas pour vous 😛.

# Projet démarré : Etunicorn

Pouvoir payer au 10⁵ avec une carte ou pouvoir pointer aux évènements du BDE sans attendre d'être trouvé dans la liste, c'est possible, c'est un projet qui avait été initié par le BDE actuel mais qui avait été abandonné. Les deux listes sont partantes pour reprendre le projet, du coup Benoît et Geoffrey ont commencé à taffer sur le projet ce week-end. Si vous voulez voir les mots d'amour qu'ils se sont envoyés par commit interposés, direction <https://archives.plil.fr/LeClubInfo/etunicorn-server> et <https://archives.plil.fr/LeClubInfo/etunicorn-android>.

