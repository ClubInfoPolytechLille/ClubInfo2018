---
author: "Geoffrey Preud'homme"
tags: ci
date: 2015-11-09
---
Oui, ça fait longtemps que j'avais pas fait de compte-rendu useless, et le pire c'est que cette fois-ci je m'y prend en avance parce que j'ai peur d'oublier certains points.

* C'EST LA PHOTOOOO ! PRENEZ DES VÊTEMENTS <del>GIS</del> GRIS !
* Débrief rapide Nuit de l'Info
* Polos & Sweats (j'préfère faire en préventif)

**Cours :** Probablement deux-trois trucs avec les PC à Polytech et bash en général (`.bashrc`, `startx`, installer un paquet en local, `cat`, `grep`, réveiller les PC, la syntaxe du bash si on a le temps).

Pour le bureau :

* Faudrait voir à créer une page Facebook. Quand même. Un jour.
* AW?
