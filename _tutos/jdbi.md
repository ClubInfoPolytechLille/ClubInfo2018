---
title: JDBI
tags: jdbi java
author: "Benoit 'badetitou' Verhaeghe"
license: WTFPL
license_url: http://www.wtfpl.net/
---

# Objectifs
L'objectif de jdbi est de simplifier l'accès au base de donner en offrant un niveau d'abstraction supplémentaire à jdbc.

Cette bibliothèque permet de se connecter à une base de données, effectuer des requetes sql sécurisées, et de convertir automatiquement les résultats en objet.

Vous trouverez ici quelques outils pour mieux comprendre jdbi.

# Annotations

| Annotations | Définitions |
| ----------- | ----------- |
| @SqlQuery(String myRequest) | A mettre au dessus du nom de la fonction voulu (pour une interface). Execute la requete de type Select defini" dans "myRequest". |
| @SqlUpdate(String myRequest) | A mettre au dessus du nom de la fonction voulu (pour une interface). Execute la requete de type Update (tout sauf les Select et les Procedures stockées) definit dans "myRequest". |
| @SqlUpdate(String myRequest) | A mettre au dessus du nom de la fonction voulu (pour une interface). Execute la requete de type Update (tout sauf les Select et les Procedures stockées) definit dans "myRequest". |
| @RegisterMapperFactory(BeanMapperFactory.class) | A mettre au dessus du nom de la fonction voulu (pour une interface). Permet de retourner l'objet correspondant au type de retour de la fonction. La correspondance se fait autmatiquement, l'object doit cependant avoir les même attributs que les colonnes retourner par la requete. |
| @GetGeneratedKeys | A mettre au dessus du nom de la fonction voulu (pour une interface). Permet de récuperer la clef primaire générée lors d'une insertion. |
